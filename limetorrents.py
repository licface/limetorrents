#!/usr/bin/env python2
#-*- encoding:utf8
#encoding:utf8

import os
import re
import sys
import clipboard
import requests
# import colorama
from unidecode import unidecode
#import termcolor
import cmdw
from texttable import Texttable
from bs4 import BeautifulSoup as bs
from configset import configset
from pydebugger.debug import debug
from proxy_tester import proxy_tester, auto
import argparse
import random
import inspect
from pause import pause
#import codecs
import urllib2
from datetime import datetime
import urlparse
import time
import traceback
import shutil
from make_colors import make_colors
import ushlex
if sys.platform == 'win32':
	import msvcrt as getch
else:
	import getch

import math
def convert_size(size_bytes):
    if (size_bytes == 0):
        return '0B'
    size_name = ("B", "KB", "MB", "GB", "TB", "PB", "EB", "ZB", "YB")
    i = int(math.floor(math.log(size_bytes, 1024)))
    p = math.pow(1024, i)
    s = round(size_bytes / p, 2)
    return '%s %s' % (s, size_name[i])
PID = os.getpid()
print "PID =", PID
import psutil
if 'linux' in sys.platform:
    MEM = convert_size(psutil.Process(int(PID)).memory_info().shared)
elif 'win32' in sys.platform:
    MEM = convert_size(psutil.Process(int(PID)).memory_info().private)
else:
    MEM = psutil.Process(int(PID)).memory_info()
print "MEM =", MEM
#reload(sys)
#if sys.stdout.encoding != 'cp850':
    #sys.stdout = codecs.getwriter('ascii')(sys.stdout, 'strict')
#if sys.stderr.encoding != 'cp850':
    #sys.stderr = codecs.getwriter('ascii')(sys.stderr, 'strict')

MAX_LENGTH = cmdw.getWidth()
DOWNLOAD_PATH=None
if os.getenv('DOWNLOAD_PATH'):
    DOWNLOAD_PATH = os.getenv('DOWNLOAD_PATH')


class limetorrents(object):

    def __init__(self, proxy = None, debug=False):
        super(limetorrents, self)
        self.URL = 'https://www.limetorrents.pro/'
        #self.URL = 'https://limetorrents.cc/'
        self.debug = debug
        self.proxy = proxy
        self.page = None
        self.configname = os.path.join(os.path.dirname(__file__), 'limetorrents.ini')
        self.CONFIG = configset(self.configname)
        # self.Session = requests.Session()
        if sys.platform == 'win32':
            import cfscrape
            self.Session = cfscrape.Session()
        else:
            self.Session = requests.Session()
        self.headers = {'user-agent':'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.87 Safari/537.36'}
        self.Session.headers.update(self.headers)
    
    def pause(self, page=''):
        lineno = str(inspect.stack()[1][2])		
        if page:
            page = make_colors("[" + str(page) + "]", "lw", "bl")
        else:
            page = make_colors("[" + str(lineno) + "]", "lw", "bl")
        note = make_colors("Enter to Continue . ", "lw", "lr") + "[" + page + "] " + make_colors("x|q = exit|quit", "lw", "lr")
        print(note)
        q = getch.getch()
        if q == 'x' or q == 'q':
            sys.exit(make_colors("EXIT !", 'lw','lr'))

    def test_proxy(self, proxies, timeout=3):
        if not proxies:
            return False
        try:
            a = requests.request('GET', self.URL, proxies=proxies, verify=False, timeout=timeout)
            return True
        except:
            return False
        
    def setProxy(self, proxies=None):
        '''
            format proxies: ['http://ip:port', 'https://ip:port']
        '''
        debug()
        PROXY = {}
        # if not self.CONFIG.read_config('PROXY', 'use_proxy'):
        #     debug(use_proxy = self.CONFIG.read_config('PROXY', 'use_proxy'))
        #     PROXY = {}
        # else:
        if proxies:  #data must list instance
            for i in proxies:
                #host, port = str(i).split(":")
                scheme = urlparse.urlparse(i).scheme
                PROXY.update({scheme: i,})
        if self.CONFIG.read_config('PROXY', 'server') and self.CONFIG.read_config('PROXY', 'port'):
            PROXY.update({
            'http': 'http://%s:%s'%(self.CONFIG.read_config('PROXY', 'server'), self.CONFIG.read_config('PROXY', 'port')),
            'https': 'https://%s:%s'%(self.CONFIG.read_config('PROXY', 'server'), self.CONFIG.read_config('PROXY', 'port'))
            })
        # print "PROXY =", PROXY
        # print "self.test_proxy(PROXY) =", self.test_proxy(PROXY)
        debug(PROXY=PROXY)
        if not self.test_proxy(PROXY):
            debug('NOT PROXY or proxies')
            pt = proxy_tester()
            list_proxy_ok = pt.test_proxy_ip(self.URL, print_list=True, limit=1)
            debug(list_proxy_ok=list_proxy_ok)
            if list_proxy_ok:
                PROXY.update({
                    'http': 'http://%s'%(list_proxy_ok[0]),
                    'https': 'https://%s'%(list_proxy_ok[0])
                })
        if not self.proxy:
            self.proxy = PROXY
        return PROXY


    #def printlist(self, defname=None, debug=None, **kwargs):
        #if not debug:
            #debug = self.debug
        #color_random_1 = [colorama.Fore.GREEN, colorama.Fore.YELLOW, colorama.Fore.LIGHTBLUE_EX, colorama.Fore.LIGHTCYAN_EX, colorama.Fore.LIGHTMAGENTA_EX, colorama.Fore.GREEN, colorama.Fore.YELLOW, colorama.Fore.LIGHTBLUE_EX, colorama.Fore.LIGHTCYAN_EX, colorama.Fore.LIGHTMAGENTA_EX, colorama.Fore.GREEN, colorama.Fore.YELLOW, colorama.Fore.LIGHTBLUE_EX, colorama.Fore.LIGHTCYAN_EX, colorama.Fore.LIGHTMAGENTA_EX,
                          #colorama.Fore.GREEN, colorama.Fore.YELLOW, colorama.Fore.LIGHTBLUE_EX, colorama.Fore.LIGHTCYAN_EX, colorama.Fore.LIGHTMAGENTA_EX, colorama.Fore.GREEN, colorama.Fore.YELLOW, colorama.Fore.LIGHTBLUE_EX, colorama.Fore.LIGHTCYAN_EX, colorama.Fore.LIGHTMAGENTA_EX, colorama.Fore.GREEN, colorama.Fore.YELLOW, colorama.Fore.LIGHTBLUE_EX, colorama.Fore.LIGHTCYAN_EX, colorama.Fore.LIGHTMAGENTA_EX]
        #colorama.init()
        #formatlist = ''
        #arrow = colorama.Fore.YELLOW + ' -> '
        #if not kwargs == {}:
            #for i in kwargs:
                ## formatlist += color_random_1[kwargs.keys().index(i)] + i + ":
                ## " + color_random_1[kwargs.keys().index(i)] +
                ## str(kwargs.get(i)) + arrow
                #formatlist += termcolor.colored((i + ": "), 'white', 'on_blue') + color_random_1[
                    #kwargs.keys().index(i)] + str(kwargs.get(i)) + arrow
        #else:
            #formatlist += random.choice(color_random_1) + " start... " + arrow
        #formatlist = formatlist[:-4]

        #if defname:
            #formatlist = make_colors(
                #defname + arrow, 'white', 'on_red') + formatlist
        #else:
            #defname = inspect.stack()[1][3]
            #formatlist = make_colors(
                #defname + arrow, 'white', 'on_red') + formatlist
        #if debug:
            #print formatlist
        #return formatlist

    def parseHeaders(self, headers):
        headers = {key:value for key, value in [re.split(": ", i) for i in re.split("\n", headers)]}
        headers.update({'accept-encoding': 'gzip, deflate'})
        # headers.update({'user-agent': self.USER_AGENT()})
        debug(headers = headers)
        return headers

    def getDetails(self, url, proxy=None, mt = 10, timeout = 5):
        # os.environ.update({'DEBUG':'1'})
        HASH = ""
        ADDED = ""
        MAGNET = ""
        TRACKERS = []
        debug(url = url)
        debug(proxy = proxy)
        headers = """accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9
accept-encoding: gzip, deflate, br
accept-language: en-US,en;q=0.9,id;q=0.8,ru;q=0.7
cache-control: max-age=0
sec-fetch-dest: document
sec-fetch-mode: navigate
sec-fetch-site: none
sec-fetch-user: ?1
upgrade-insecure-requests: 1
user-agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.92 Safari/537.36"""

        headers = self.parseHeaders(headers)
        debug(headers = headers)
        self.Session.headers.update(headers)
        nt = 0
        while 1:
            try:
                # debug(url = unidecode(url))
                # a = self.Session.get(url, proxies=proxy, timeout = timeout)
                debug(headers = headers)
                debug(proxy = proxy)
                debug(timeout = timeout)
                debug(url = url)
                if not isinstance(proxy, dict):
                    proxy = {}
                else:
                    self.Session.proxies.update(proxy)
                print(make_colors("copy url to clipboard ...", 'b', 'ly'))
                clipboard.copy(url)
                # a = requests.get(url, proxies=proxy, timeout = timeout, headers = headers)
                a = self.Session.get(url, timeout = timeout)
                break
            except:
                print(traceback.format_exc())
                debug(nt = nt)
                if nt == mt:
                    print(make_colors("Failed to Get details !", 'lw', 'r'))
                    return MAGNET, HASH, ADDED, TRACKERS
                nt += 1
                sys.stdout.write(".")
                sys.stdout.flush()
                time.sleep(1)
        b = bs(a.text, 'lxml')
        nt = 0
        error = False
        while 1:
            try:
                div_content = b.find('div', id='content').find('div', {'class':'torrentinfo'}).find('table')
                debug(div_content=div_content)
                break
            except:
                nt += 1
                debug(div_content=div_content)
                if nt == mt:
                    print(make_colors("Failed to Get magnet !", 'lw', 'r'))
                    error = True
                    break
        if error:
            return MAGNET, HASH, ADDED, TRACKERS
        if div_content:
            hash_parent = div_content.find('b', text=re.compile('Hash')).parent
            if hash_parent:
                hash_all_td = hash_parent.parent.find_all('td')
                if hash_all_td:
                    if len(hash_all_td) > 1:
                        HASH = hash_all_td[1].text
                        debug(HASH=HASH)
            added_parent = div_content.find('b', text=re.compile('Added')).parent
            if added_parent:
                added_all_td = added_parent.parent.find_all('td')
                if added_all_td:
                    if len(added_all_td) > 1:
                        ADDED = added_all_td[1].text
                        debug(ADDED=ADDED)

        div_downloadarea = b.find_all('div', {'class':'downloadarea'})[1].find('div', {'class':'dltorrent'})
        debug(div_downloadarea=div_downloadarea)
        
        if div_downloadarea:
            MAGNET = div_downloadarea.find('p').find('a').get('href')
            debug(MAGNET=MAGNET)

        
        trackerstable = b.find('table', id='trackerstable').find_all('tr')[1:]
        debug(trackerstable=trackerstable)
        for i in trackerstable:
            trackers = {}
            all_td = i.find_all('td')
            tracker = all_td[0].text
            last_check = all_td[1].text
            status = all_td[2].find('span').text
            seeders = all_td[3].text
            leechers = all_td[4].find('span').text
            trackers = {'tracker':tracker, 'last_check':last_check, 'status':status, 'seeders':seeders, 'leechers':leechers}
            TRACKERS.append(trackers)
        debug(TRACKERS=TRACKERS)
            
        # div_all_fileline = b.find_all('div', {'class':'fileline'})
        # debug(div_all_fileline=div_all_fileline)

        # for i in div_all_fileline:
        # os.environ.update({'DEBUG':'0'})
        return MAGNET, HASH, ADDED, TRACKERS

    def colored_healt(self, data):
        #colorama.init()
        if data <= 3 or data == '1' or data == '2' or data == '3':
            return make_colors(data, 'red', 'on_white')
            #return colorama.Back.RED + data
        elif data == 4 or data == '4':
            return make_colors(data, 'red', 'on_grey')
        elif data == 5 or data == '5':
            return make_colors(data, 'white', 'on_magenta')
        elif data == 6 or data == '6':
            return make_colors(data, 'white', 'on_blue')
        elif data == 7 or data == '7':
            return make_colors(data, 'red', 'on_cyan')
        elif data == 8 or data == '8':
            return make_colors(data, 'red', 'on_green')
        elif data == 9 or'9':
            return make_colors(data, 'red', 'on_green')
        elif data == 10 or data == '10':
            return make_colors(data, 'white', 'on_red')

    def download2(self, url, downloadPath='.', overwrite=False, name=None):
        debug(url=url)
        debug(name=name)
        debug(overwrite=overwrite)
        if downloadPath == '.':
            downloadPath = os.getcwd()
        if sys.platform == 'win32':
            cf = cfscrape.create_scraper()
        else:
            cf = self.Session()
        if not name:
            name = urlparse.urlparse(url).path.split('/')[-1]
            if downloadPath:
                name = os.path.join(downloadPath, name)
        if not os.path.splitext(name)[1] == '.torrent':
            name = name + '.torrent'
        if overwrite:
            if os.path.isfile(name):
                os.remove(name)
            else:
                print make_colors("PASS downloading [EXISTS]:", 'white', 'red'), make_colors(name, 'green')
                return name

        #print "[%s] " % str(PID) + "downloading:", name
        print make_colors("[%s]" % str(PID), 'magenta') + " " + make_colors("downloading:", 'cyan'), make_colors(name, 'lightred')
        r = cf.get(url, stream=True)
        with open(name, 'wb') as f:
            shutil.copyfileobj(r.raw, f)
        return name

    def download0(self, url=None, downloadPath=".", overwrite=False, name=None, response=None, proxy=None):
        try:
            from progressbar import ProgressBar, Bar, Percentage, FileTransferSpeed, ETA
            class ReFileTransferSpeed(FileTransferSpeed):
                def update(self, pbar):
                    if 45 < pbar.percentage() < 80:
                        return "Bigger Now" + FileTransferSpeed.update(self, pbar)
                    else:
                        return FileTransferSpeed.update(self, pbar)
            widgets = [ReFileTransferSpeed(), "<<<", Bar(), ">>>", Percentage(), ' ', ETA()]
            pbar = ProgressBar(widgets=widgets, max_value=100)
        except:
            pass

        CHUNK_SIZE = 32768
        global DOWNLOAD_PATH
        if DOWNLOAD_PATH:
            downloadPath = DOWNLOAD_PATH
        if not os.path.exists(downloadPath):
            qd = raw_input('DOWNLOAD_PATH is NOT EXISTS !, Create it ? [y/n]: ')
            if qd == 'y':
                os,makedirs(DOWNLOAD_PATH)
            else:
                if downloadPath == ".":
                    downloadPath = os.getcwd()
                else:
                    downloadPath = os.getcwd()
        debug(downloadPath=downloadPath)
        dest_path = datetime.strftime(datetime.now(), '%Y%m%d_%H%M%S') + ".torrent"
        if name:
            dest_path = os.path.join(downloadPath, name)
        if not os.path.splitext(dest_path)[1] == '.torrent':
            dest_path = dest_path + '.torrent'
        if isinstance(proxy, list):
            proxy = self.setProxy(proxy)
        if url:
            response = self.Session.get(url, proxies=proxy, stream=True)
        debug(dest_path=dest_path)
        if isinstance(response, requests.models.Response):
            with open(dest_path, 'wb') as f:
                pbar.start()
                for chuck in response.iter_content(CHUNK_SIZE):
                    try:
                        pbar.update(1)
                    except:
                        pass
                    if chuck:
                        f.write(chuck)
            pbar.finish()

    def download1(self, url, downloadPath = ".", overwrite=False, name = None):
        import mimelist
        global DOWNLOAD_PATH
        if DOWNLOAD_PATH:
            downloadPath = DOWNLOAD_PATH
        if not os.path.exists(downloadPath):
            qd = raw_input('DOWNLOAD_PATH is NOT EXISTS !, Create it ? [y/n]: ')
            if qd == 'y':
                os,makedirs(DOWNLOAD_PATH)
            else:
                if downloadPath == ".":
                    downloadPath = os.getcwd()
                else:
                    downloadPath = os.getcwd()
        debug(downloadPath=downloadPath)
        hdr = {'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.11 (KHTML, like Gecko) Chrome/23.0.1271.64 Safari/537.11',
               'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
               'Accept-Charset': 'ISO-8859-1,utf-8;q=0.7,*;q=0.3',
               'Accept-Encoding': 'none',
               'Accept-Language': 'en-US,en;q=0.8',
               'Connection': 'keep-alive'}        

        rq = urllib2.Request(url, headers= hdr)
        try:
            res = urllib2.urlopen(rq)
        except urllib2.HTTPError, e:
            print e.fp.read()
        if res:
            content = res.read()
            head = res.headers
            if not name:
                mime = head.get('content-type')
                mime = mimelist.get(str(mime))
                if mime:
                    filename = os.path.splitext(os.path.basename(url))[0] + "." + mime[0]
                else:
                    filename = datetime.strftime(datetime.now(), '%Y%m%d_%H%M%S') + ".torrent"
            else:
                filename = name

            filedownload = open(downloadPath + "/" + filename, "wb")
            filedownload.write(res.read())
            filedownload.close()        

    def download2(self, url, downloadPath=".", overwrite=False, name=None, proxy=None):
        import wget
        debug()
        debug(url=url)
        global DOWNLOAD_PATH
        if DOWNLOAD_PATH:
            downloadPath = DOWNLOAD_PATH
        if not os.path.exists(downloadPath):
            qd = raw_input('DOWNLOAD_PATH is NOT EXISTS !, Create it ? [y/n]: ')
            if qd == 'y':
                os,makedirs(DOWNLOAD_PATH)
            else:
                if downloadPath == ".":
                    downloadPath = os.getcwd()
                else:
                    downloadPath = os.getcwd()
        debug(downloadPath=downloadPath)
        dest_path = datetime.strftime(datetime.now(), '%Y%m%d_%H%M%S') + ".torrent"
        if name:
            dest_path = os.path.join(downloadPath, name)
        if not os.path.splitext(dest_path)[1] == '.torrent':
            dest_path = dest_path + '.torrent'
        if isinstance(proxy, list):
            proxy = self.setProxy(proxy)
        debug(dest_path=dest_path)
        name = wget.download(url, dest_path)
        debug(name=name)

    def download(self, url='', downloadPath=".", overwrite=None, name=None, proxy=None):
        debug()
        debug(url=url)
        global DOWNLOAD_PATH
        if DOWNLOAD_PATH:
            downloadPath = DOWNLOAD_PATH
        if not os.path.exists(downloadPath):
            qd = raw_input('DOWNLOAD_PATH is NOT EXISTS !, Create it ? [y/n]: ')
            if qd == 'y':
                os.makedirs(DOWNLOAD_PATH)
            else:
                if downloadPath == ".":
                    downloadPath = os.getcwd()
                else:
                    downloadPath = os.getcwd()
        debug(downloadPath=downloadPath)
        import urlparse
        
        if os.getenv('http_proxy'):
                proxy.update({'http':os.getenv('http_proxy')})
        elif os.getenv('https_proxy'):
            proxy.update({'https':os.getenv('https_proxy')})
        else:
            if self.proxy:
                proxy = self.setProxy(proxy)
            if proxy:
                proxy = self.setProxy(proxy)
        debug(proxy=proxy)
        hdr = {'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.11 (KHTML, like Gecko) Chrome/23.0.1271.64 Safari/537.11',
               'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
               'Accept-Charset': 'ISO-8859-1,utf-8;q=0.7,*;q=0.3',
               'Accept-Encoding': 'none',
               'Accept-Language': 'en-US,en;q=0.8',
               'Connection': 'keep-alive'}
        try:
            # b = a.find_next('a', {'id': 'downloadButton', })
            # fullURL = self.subsceneURL + b.get('href')
            if url:
                rq = urllib2.Request(url, headers=hdr)

                debug(rq=rq)
                if proxy:
                    if urlparse.urlparse(url).scheme == 'http' and proxy.get('http') != None:
                        rq.set_proxy(proxy.get('http'), 'http')
                    if urlparse.urlparse(url).scheme == 'https' and proxy.get('https') != None:
                        rq.set_proxy(proxy.get('http'), 'https')
                if proxy:
                    debug(proxy=proxy)
                    proxy = urllib2.ProxyHandler(proxy)
                    opener = urllib2.build_opener(proxy)
                    urllib2.install_opener(opener)

                res = urllib2.urlopen(rq)

                while True:
                    if res.code != 200:
                        res = urllib2.urlopen(rq)
                    else:
                        break
                head = res.headers
                filename = re.split(
                    "; |filename=", head.values()[1])[-1].strip()
                if name:
                    filename = name + os.path.splitext(filename)[1]
                filename = re.sub(": |:", " - ", str(filename))
                debug(filename=filename)
                while True:
                    if str(filename).strip() == '-1':
                        rq = urllib2.Request(fullURL, headers=hdr)
                        print "Proxy is SET:", rq.has_proxy()
                        if not proxy == {}:
                            if urlparse.urlparse(url).scheme == 'http' and proxy.get('http') != None:
                                rq.set_proxy(proxy.get('http'), 'http')
                            if urlparse.urlparse(url).scheme == 'https' and proxy.get('https') != None:
                                rq.set_proxy(proxy.get('http'), 'https')
                        res = urllib2.urlopen(rq)
                        while True:
                            if res.code != 200:
                                res = urllib2.urlopen(rq)
                            else:
                                break
                        head = res.headers
                        filename = re.split(
                            "; |filename=", head.values()[1])[-1].strip()
                        filename = re.sub(": |:", " - ", str(filename))
                        if name:
                            filename = name + os.path.splitext(filename)[1]
                    else:
                        break
                if not os.path.exists(downloadPath):
                    os.makedirs(downloadPath)
                #print "filename =", filename
                if os.path.exists(downloadPath + "/" + filename):
                    if overwrite:
                        print make_colors("downloading:", 'cyan'), make_colors(filename, 'lightred')
                        filedownload = open(
                            downloadPath + "/" + filename, "wb")
                        filedownload.write(res.read())
                        filedownload.close()
                    else:
                        print make_colors("PASS downloading [EXISTS]:", 'white', 'lightred'), make_colors(filename, 'green')
                else:
                    print make_colors("[%s]" % str(PID), 'magenta') + " " + make_colors("downloading:", 'cyan'), make_colors(filename, 'lightred')
                    # filedownload = open(downloadPath + "\\" + filename, "wb")
                    if name:
                        filedownload = open(os.path.join(
                            downloadPath, str(filename).strip()), "wb")
                    else:
                        filedownload = open(os.path.join(
                            downloadPath, str(filename).strip()[1:]), "wb")
                    filedownload.write(res.read())
                    filedownload.close()
        except:
            import traceback
            debug(error=traceback.format_exc())
            #return termcolor.colored(data, 'red', 'on_yellow')

    def re_string(self, string):
        ss = []
        s = ushlex.split(string)
        if len(s) > 1:
            for i in s:
                ss.append(unicode(i))
            return " ".join(ss)
        else:
            return str(s[0])
        
    def makeTable(self, data_search):
        table = Texttable()
        table.set_cols_align(["l", "l", "c", "l", "c", "l", "c"])
        table.set_cols_valign(["t", "t", "t", "t", "m", "m", "m"])
        table.set_cols_width([
            int(MAX_LENGTH * 0.02),
            int(MAX_LENGTH * 0.5),
            int(MAX_LENGTH * 0.05),
            int(MAX_LENGTH * 0.02),
            int(MAX_LENGTH * 0.03),
            int(MAX_LENGTH * 0.1),
            int(MAX_LENGTH * 0.02),
        ])
        # table.header(['No','Name','Lang','Owner', 'Rated'])
        table.header(['No', 'Name', 'Size', 'Seed', 'Leech',
                      'Added', 'Healt'])
        sys.dont_write_bytecode = True
        number = 0
        # m: {
        #                     'download_link': download_link,
        #                     'title': td_all[1].text.strip(),
        #                     'fps': td_all[2].text.strip(),
        #                     'cds': td_all[3].text.strip(),
        #                     'lang': td_all[4].text.strip(),
        #                     'contributor': td_all[5].text.strip(),
        #                     'set': td_all[6].text.strip(),
        #                     'counts': td_all[7].text.strip(),
        #                     'comment': td_all[8].text.strip(),
        #                     # 'rating': td_all[9].text.strip(),
        #                     'rating': rating,
        #                     'uploaded': td_all[10].text.strip(),
        #                 }
        for i in data_search:
            name = data_search.get(i).get('name')
            size = data_search.get(i).get('size')
            seed = data_search.get(i).get('seed')
            leech = data_search.get(i).get('leech')
            added = data_search.get(i).get('uploaded')
            healt = data_search.get(i).get('healt')[0][-1]

            number += 1
            #try:
            #table.add_row([str(number), self.re_string(name), self.re_string(size), self.re_string(seed), self.re_string(leech), self.re_string(added), self.re_string(healt)])
            table.add_row([str(number), name, size, seed, leech, added, healt])
            #except:
                #table.add_row([str(number), unicode(name).encode('utf-8', errors='replace'), size, seed, leech, added, healt])
            #table.add_row([str(number), unicode(name).encode(sys.stdout.encoding, errors='replace'), unicode(size).encode(sys.stdout.encoding, errors='replace'), unicode(seed).encode(sys.stdout.encoding, errors='replace'), unicode(leech).encode(sys.stdout.encoding, errors='replace'), unicode(added).encode(sys.stdout.encoding, errors='replace'), unicode(healt).encode(sys.stdout.encoding, errors='replace')])
        
        print table.draw()

        return data_search, number

    def makeList(self, alist, ncols, vertically=True, file=None):
        from distutils.version import StrictVersion  # pep 386
        import prettytable as ptt  # pip install prettytable
        assert StrictVersion(ptt.__version__) >= StrictVersion(
            '0.7')  # for PrettyTable.vrules property
        L = alist
        nrows = - ((-len(L)) // ncols)
        ncols = - ((-len(L)) // nrows)
        t = ptt.PrettyTable([str(x) for x in range(ncols)])
        t.header = False
        t.align = 'l'
        t.hrules = ptt.NONE
        t.vrules = ptt.NONE
        r = nrows if vertically else ncols
        chunks = [L[i:i + r] for i in range(0, len(L), r)]
        chunks[-1].extend('' for i in range(r - len(chunks[-1])))
        if vertically:
            chunks = zip(*chunks)
        for c in chunks:
            t.add_row(c)
        print make_colors(t, 'green')

    def search(self, query, stype=all, url_query=None, proxy=None, **kwargs):
        '''
            stype: all, anime, applications, games, movies, music, tv, other
        '''
        
        if self.proxy:
            proxy = self.setProxy(self.proxy)
        elif proxy:
            proxy = self.setProxy(proxy)
        else:
            proxy = {}

        debug(url_query = url_query)
        debug(query = query)

        if " " in query:
            query0 = query.replace(" ", "-")
            query = query0
        debug(query = query)
        debug(stype = stype)
        if not url_query:
            url = 'search/{0}/{1}'.format(stype, query)
            URL = self.URL + url
        else:
            URL = url_query

        result_search = {}
        
        def get_a():
            n = 1
            a = None
            while 1:
                try:
                    a = self.Session.get(URL, timeout=60, proxies=proxy, **kwargs)
                    break
                except:
                    if self.debug:
                        traceback.format_exc(print_msg= False)
                    sys.stdout.write(".")
                    time.sleep(1)
                    n += 1
                    if n == 50:
                        break
            if a:
                return a
            return False

        
        debug(URL = URL)
        debug(proxy=proxy)
        
        while 1:
            try:
                a = self.Session.get(URL, proxies=proxy, timeout=5)
                break
            except:
                #print(make_color("Connection Broken. Please wait reconnecting ...."
                time.sleep(1)
        #debug(a_content=a.content, debug=self.debug)
        b = bs(a.content, 'lxml')
        page = self.pagination(a.content, stype)
        debug(page = page)
        # pause()
        c = b.find('table', {'class': 'table2'})
        # debug(c=c)
        # c1 = c.find_all('td', {'class': re.compile('td.*')})

        if not c:
            return None, page
        c1 = c.find_all('tr')
        if not c1:
            return None

        # debug(c1=c1)
        # debug(len_c1=len(c1))
        # debug(c1_4=c1[4])
        # debug(c1_last=c1[-1])

        n = 1
        for i in c1:
            d = i.find_all('td', {'class': re.compile('td.*')})
            if d == []:
                pass
            else:
                debug(d=d)
                if len(d) > 1:
                    download_link = d[0].find('a', {'class': 'csprite_dl14'}).get('href')
                    name = d[0].find_all('a')[1].text
                    link = d[0].find_all('a')[1].get('href')
                    result_search.update(
                        {
                            n: {
                                'name': name,
                                'longname': name,
                                'link': link,
                                'download_link': download_link,
                                'uploaded': d[1].text.strip(),
                                'size': d[2].text.strip(),
                                'seed': d[3].text.strip(),
                                'leech': d[4].text.strip(),
                                'healt': d[5].find('div').get('class'),
                            }
                        }
                    )
                    n += 1

                # print "-" * 220

        debug(result_search=result_search)

        self.makeTable(result_search)
        # pause()
        if page:
            debug(page = page)
            page_list0 = []
            page_list = ""
            for i in page:
                page_list0.append(i)
            page_list0 = sorted(page_list0)
            for i in page_list0:
                if str(i).isdigit():
                    page_list += str(i) + " | "
            page_list = "  p[revious] " + page_list + " n[ext]"
            print page_list
        
        return result_search, page

        # debug(c1=c1)

    def home_category(self, url, proxy=None, **kwargs):
        '''
            stype: all, anime, applications, games, movies, music, tv, other
        '''
        
        if self.proxy:
            proxy = self.setProxy(self.proxy)
        if proxy:
            proxy = self.setProxy(proxy)
        else:
            proxy = {}
        result_search = {}
        a = self.Session.get(url, proxies=proxy, **kwargs)
        # debug(a_content=a.content)
        b = bs(a.content, 'lxml')
        page = self.pagination(a.content)
        c = b.find('table', {'class': 'table2'})
        # debug(c=c)
        # c1 = c.find_all('td', {'class': re.compile('td.*')})
        if not c:
            return None, page
        c1 = c.find_all('tr')
        if not c1:
            return None
        # debug(c1=c1)
        # debug(len_c1=len(c1))
        # debug(c1_4=c1[4])
        # debug(c1_last=c1[-1])
        n = 1
        for i in c1:
            d = i.find_all('td', {'class': re.compile('td.*')})
            if d == []:
                pass
            else:
                # debug(d=d)
                download_link = d[0].find(
                    'a', {'class': 'csprite_dl14'}).get('href')
                name = d[0].find_all('a')[1].text
                link = d[0].find_all('a')[1].get('href')
                result_search.update(
                    {
                        n: {
                            'name': name,
                            'link': link,
                            'download_link': download_link,
                            'uploaded': d[1].text.strip(),
                            'size': d[2].text.strip(),
                            'seed': d[3].text.strip(),
                            'leech': d[4].text.strip(),
                            'healt': d[5].find('div').get('class'),
                        }
                    }
                )
                n += 1

                # print "-" * 220
        # debug(result_search=result_search)
        self.makeTable(result_search)
        if page:
            page_list0 = []
            page_list = ""
            for i in page:
                page_list0.append(i)
            page_list0 = sorted(page_list0)
            for i in page_list0:
                if str(i).isdigit():
                    page_list += str(i) + " | "
            page_list = "  p[revious] " + page_list + " n[ext]"
            print page_list
        return result_search, page

        # debug(c1=c1)
    
    def pagination(self, html_beautifulsoup, stype="all"):
        if not stype:
            stype = "all"
        a = bs(html_beautifulsoup, 'lxml')
        b = a.find('div', {'class': 'search_stat'})
        if not b:
            return None
        debug(b=b)
        b1 = b.find_all('a')
        page = {}
        for i in b1:
            if not stype in i.get('href'):
                page.update(
                    {
                        i.text: i.get('href').replace("//", "/").replace("search", "search/{}".format(stype))[1:]
                    }
                )
            else:
                page.update(
                    {
                        i.text: i.get('href').replace("//", "/")[1:]
                    }
                )
        debug(page = page)
        return page

    def home(self, nlist=3, stype = None, proxy=None, timeout=3):
        MAX_WIDTH = cmdw.getWidth()
        if MAX_WIDTH < 120:
            nlist = 1
        #print("MAX_WIDTH =", MAX_WIDTH)
        all_result = {}
        n = 1
        def category(c1, n=1, color = 'yellow'):
            result_search = {}
            result_search_list = []
            if not c1:
                return None
            # n = 1
            # debug(c1=c1)
            for i in c1:
                debug(i = i)
                d = i.find_all('td', {'class': re.compile('td.*')})
                #debug(d = d)
                #debug(d_0 = d[0])
                if d == []:
                    pass
                else:
                    #debug(d_0 = d[0])
                    if d[0].find('a', {'class': 'csprite_dl14'}):
                        try:
                            download_link = d[0].find('a', {'class': 'csprite_dl14'}).get('href')
                            debug(download_link = download_link)
                        except:
                            #debug(c1 = c1)
                            #debug(i = i)
                            #debug(d = d)
                            self.pause()
                        name = d[0].find_all('a')[1].text
                        name1 = d[0].find_all('a')[1].text
                        if nlist >= 3:
                            if len(name) >= 38:
                                name = name[0:38] + " ..."
                        link = d[0].find_all('a')[1].get('href')
                        result_search.update(
                            {
                                n: {
                                    'name': name,
                                    'longname': name1,
                                    'link': link,
                                    'download_link': download_link,
                                    'size': d[1].text.strip(),
                                    'seed': d[2].text.strip(),
                                    'leech': d[3].text.strip(),
                                    'healt': d[4].find('div').get('class'),
                                }
                            }
                        )
                        result_search_list.append(str(n) + ". " + make_colors(name, color) +
                                                  make_colors(" [", 'red') +
                                                  make_colors("{0}".format(d[1].text.strip()), 'white', 'on_red') +
                                                  "|" +
                                                  make_colors("{0}".format(d[2].text.strip()), 'white', 'on_yellow') +
                                                  "|" +
                                                  make_colors("{0}".format(d[3].text.strip()), 'white', 'on_cyan') +
                                                  "|" +
                                                  make_colors("{0}".format(d[4].find('div').get('class')[0][-1]), 'white', 'on_blue') +
                                                  make_colors("]", 'red'))  #.format(
                            #d[1].text.strip(), d[2].text.strip(), d[3].text.strip(), d[4].find('div').get('class')[0][-1]))
                        n += 1
            debug(result_search=result_search)
            all_result.update(result_search)
            return result_search, result_search_list, n

        ######### movie category ##########
        URL = self.URL + 'home/'
        #debug(URL=URL)
        ns = 1
        ns_max_try = 10
        ns_error = False
        sess, list_proxy = None, None
        if proxy == 'auto':
            sess, list_proxy = auto.auto(self.Session, self.URL, limit = 10)
            debug(list_proxy = list_proxy)
        if not stype:
            if proxy == None:
                proxy = {}
            np = 0
            while 1:
                try:
                    if proxy=='auto' and list_proxy:
                        a = self.Session.get(URL, proxies=list_proxy[np], timeout=timeout)
                    else:
                        if proxy=='auto':
                            proxy = {}
                        a = self.Session.get(URL, proxies=proxy, timeout=timeout)
                        
                    #debug(a_content=a.content)
                    break
                except:
                    if proxy == 'auto' and list_proxy and np == len(list_proxy) - 1:
                        break
                    else:
                        np+=1   
                    
                    if ns == ns_max_try:
                        ns_error = True
                        break
                    else:
                        sys.stdout.write(".")
                        time.sleep(1)
                        ns+=1
            if ns_error:
                sys.exit(make_colors("Max Try Attempt of not stype !", 'white', 'on_red'))
            #self.pause()
            b = bs(a.text, 'lxml')
            page = self.pagination(a.content)
            c = b.find('div', id='content').find_all('table', {'class': 'table2'})
            debug(c=c)
            for i in c:
                cats = i.find('a', href=re.compile('browse-torrents'))
                #debug(cats = cats)
            #self.pause()
            # c1 = c.find_all('td', {'class': re.compile('td.*')})
            if not c:
                return None, page
            movies_result_search, movies_result_search_list, n = category(c[0].find_all('tr'), n)
            print "Movies Torrrent Update"
            self.makeList(movies_result_search_list, nlist)
    
            ######### TV Shows category ##########
            tv_result_search, tv_result_search_list, n = category(c[1].find_all('tr'), n, 'lightwhite')
            print "TV Shows Torrrent Update"
            self.makeList(tv_result_search_list, nlist)
    
            ######### Musics Shows category ##########
            musics_result_search, musics_result_search_list, n = category(c[2].find_all('tr'), n, 'lightcyan')
            print "Musics Shows Torrrent Update"
            self.makeList(musics_result_search_list, nlist)
    
            ######### Games Shows category ##########
            games_result_search, games_result_search_list, n = category(c[3].find_all('tr'), n, 'lightblue')
            print "Games Shows Torrrent Update"
            self.makeList(games_result_search_list, nlist)
    
            ######### Applications Shows category ##########
            applications_result_search, applications_result_search_list, n = category(c[4].find_all('tr'), n, 'lightgreen')
            print "Applications Shows Torrrent Update"
            self.makeList(applications_result_search_list, nlist)
    
            ######### Anime Shows category ##########
            anime_result_search, anime_result_search_list, n = category(c[5].find_all('tr'), n, 'lightmagenta')
            print "Anime Shows Torrrent Update"
            self.makeList(anime_result_search_list, nlist)
    
            ######### Other Shows category ##########
            others_result_search, others_result_search_list, n = category(c[6].find_all('tr'), n, 'lightred')
            print "Others Shows Torrrent Update"
            self.makeList(others_result_search_list, nlist)

        else:
            stype_dict = {
                'movies': 'Movies',
                'tv': 'TV-shows',
                'music': 'Music',
                'games': 'Games',
                'applications': 'Applications',
                'anime': 'Anime',
                'other': 'Other',
            }
            atype = stype_dict.get(stype)
            if not atype:
                return None, None
            URL = self.URL + 'browse-torrents/' + atype
            return self.home_category(URL)
            #return self.home_category(URL)
            
        if page:
            page_list0 = []
            page_list = ""
            for i in page:
                page_list0.append(i)
            page_list0 = sorted(page_list0)
            for i in page_list0:
                if str(i).isdigit():
                    page_list += str(i) + " | "
            page_list = "  p[revious] " + page_list + " n[ext]"
            print page_list
        return all_result, page

    def navigator(self, query, stype, url_query=None, downloadPath=".", overwrite=None, home = False, nlist = 3, page_return = None, proxy=None):
        if cmdw.getWidth() < 130:
            nlist = 1
        
        debug(home = home)
        debug(query = query)
        debug(proxy=proxy)
        debug(nlist=nlist)

        if self.proxy:
            proxy = self.proxy
        if downloadPath == ".":
            downloadPath = os.getcwd()
        #print "PROXY =", proxy
        if not home:
            if stype == None:
                stype = 'all'
        if query:
            data_result, page = self.search(query, stype, url_query, proxy)
            debug(data_result1 = data_result)
            debug(page = page)
        else:
            data_result, page = self.home(nlist, stype, proxy)
            #debug(data_result2 = data_result)
        debug(data_result = data_result)
        # debug(page=page)
        # debug(type_page=type(page))
        q1 = ''
        if not page_return:
            if page:
                q1 = raw_input('[%s:%s] Select Number to Download [p = previous, n = next, x = goto page, c = change category, s = search, h = home, m[number of list] = magnet, q = quit]: ' % (PID, MEM))
                self.page = page
            elif self.page:
                q1 = raw_input('[%s:%s] Select Number to Download [p = previous, n = next, x = goto page, c = change category, s = search, h = home, m[number of list] = magnet, q = quit]: ' % (PID, MEM))
                page = self.page
            else:
                q1 = raw_input('[%s:%s] Select Number to Download [c = goto category of page, s = search, h = home, N[number of list] = number each of list, m[number of list] = magnet, q = quit]: ' % (PID, MEM))
        else:
            page = page_return
            self.page = page

        debug(q1 = q1)
        if q1 == '':
            if not data_result:
                return self.navigator('', None, url_query, downloadPath, overwrite, True, nlist, page_return, proxy)    
            return self.navigator(query, stype, url_query, downloadPath, overwrite, home, nlist, page_return, proxy)
        while 1:
            if q1:
                # os.environ.update({'DEBUG':'1'})
                q1 = str(q1).strip()
                debug(q1 = q1)

                if str(q1) == 'q':
                    sys.exit(0)
                elif str(q1) == 'h':
                    return self.navigator('', None, url_query, downloadPath, overwrite, True, nlist, proxy=proxy)
                elif str(q1).strip()[0] == 'm' and len(str(q1).strip()) > 1 and q1[1:4].isdigit():
                    debug('GET_DETAILS')
                    if data_result:
                        link = data_result.get(int(str(q1).strip()[1:])).get('link')
                        link = self.URL[:-1] + link
                        debug(link=link)
                        _magnet, _hash, _added, _trackers = self.getDetails(link, proxy)
                        if _magnet:
                            clipboard.copy(_magnet)
                        debug(magnet=_magnet)
                        debug(_hash=_hash)
                        debug(added=_added)
                        debug(trackers=_trackers)
                    else:
                        debug(data_result="NO data_result !")
                        print "NO data_result !"
                    return self.navigator(query, stype, url_query, downloadPath, overwrite, home, nlist, page_return, proxy)
                elif str(q1).strip()[0] == 'N' and len(str(q1).strip()) > 1 and q1[1:4].isdigit():
                    nlist = int(str(q1).strip()[1:])
                    return self.navigator(query, stype, url_query, downloadPath, overwrite, home, nlist, proxy=proxy)
                elif str(q1).strip() == '':
                    return self.navigator('', stype, url_query, downloadPath, overwrite, home, nlist, proxy=proxy)
                debug(page = page)
                if page:
                    if str(q1) == 'n':
                        debug(self_URL = self.URL)
                        debug(Next_page = page.get('Next page'))
                        url_query = self.URL + page.get('Next page')
                        debug(url_query_next=url_query)
                        if url_query == '#':
                            pass
                        else:
                            #return self.navigator(query, stype, url_query)
                            return self.navigator(query, stype, url_query, downloadPath, overwrite, home, nlist, proxy=proxy)
    
                    if str(q1) == 'p':
                        url_query = self.URL + page.get('Previous page')
                        # debug(url_query_previous=url_query)
                        if url_query == '#':
                            pass
                        elif url_query is None or url_query == '':
                            url_query = page.get(str(q1))
                        else:
                            #return self.navigator(query, stype, url_query, proxy=proxy, verify=verify)
                            return self.navigator(query, stype, url_query, downloadPath, overwrite, home, nlist, proxy=proxy)

                    if str(q1) == 'x':
                        q = raw_input('Goto Page number: ')
                        if str(q).strip() in page:
                            url_query = self.URL + page.get(str(q))
                            #return self.navigator(query, stype, url_query, proxy=proxy, verify=verify)
                            return self.navigator(query, stype, url_query, downloadPath, overwrite, home, nlist, proxy=proxy)

                if str(q1) == 'c':
                    category = {
                        1: 'all',
                        2: 'anime',
                        3: 'applications',
                        4: 'games',
                        5: 'movies',
                        6: 'music',
                        7: 'tv',
                        8: 'other'

                    }
                    category_list = []
                    for i in category:
                        category_list.append(
                            str(i) + ". " + category.get(i).title())
    
                    self.makeList(category_list, 2)
                    cq = raw_input('Select Category: ')
                    if str(cq).isdigit():
                        #return self.navigator(query, category.get(int(cq)), home = home)
                        return self.navigator(query, category.get(int(cq)), url_query, downloadPath, overwrite, home, nlist, None, proxy)
                if str(q1) == 's':
                    qs = raw_input("search: ")
                    if qs:
                        return self.navigator(qs, stype, url_query, downloadPath, overwrite, False, nlist, proxy=proxy)
                if str(q1) == 'h':
                    return self.navigator(query, None, url_query, downloadPath, overwrite, True, nlist, proxy=proxy)

                debug(data_result = data_result)
                if data_result:
                    if not home and page:
                        self.page = page
                    if str(q1).isdigit() and int(q1) <= len(data_result):
                        download_link = data_result.get(int(q1)).get('download_link')
                        #debug(download_link=download_link)
                        #debug(data_result = data_result)
                        #debug(type_data_result = type(data_result))
                        name = data_result.get(int(q1)).get('longname')
                        debug(process = "str(q1).isdigit() and int(q1) <= len(data_result)", name = name, download_link = download_link)
                        self.download(
                            download_link, downloadPath, overwrite, name=name)
                        #return self.navigator(query, stype, url_query, downloadPath, overwrite, proxy, home, nlist, verify)
                        return self.navigator(query, stype, url_query, downloadPath, overwrite, home, nlist)
                    elif str(q1).lower() == 'all' or str(q1).lower() == 'a':
                        debug(process = "str(q1).lower() == 'all' or str(q1).lower() == 'a'")
                        for i in data_result:
                            name = data_result.get(i).get('longname')
                            self.download(data_result.get(i).get(
                                'download_link'), downloadPath, overwrite, name=name)
                        #return self.navigator(query, stype, url_query, downloadPath, overwrite, proxy, home, nlist, verify)
                        return self.navigator(query, stype, url_query, downloadPath, overwrite, home, nlist, proxy=proxy)
                    else:
                        if "," in str(q1) or "|" in str(q1) or "/" in str(q1) or "\\" in str(q1):
                            q3 = re.split(",|/", str(q1).strip())
                            q11 = []
                            for f in q3:
                                if str(f).strip() != '':
                                    q11.append(str(f).strip())
                            q2 = q11
                        elif "-" in str(q1).strip():
                            fr, to = str(q1).split("-")
                            q11 = []
                            for x in range(int(fr.strip()), int(to.strip()) + 1):
                                q11.append(str(x).strip())
                            q2 = q11
                        else:
                            home = False
                            debug(q1 = q1)
                            debug(stype = stype)
                            debug(url_query = url_query)
                            debug(overwrite = overwrite)
                            debug(downloadPath = downloadPath)
                            debug(home = home)
                            debug(nlist = nlist)
                            #debug(kwargs = kwargs)

                            if stype == None:
                                stype = 'all'
                            #data_result, page = self.search(query, stype, url_query)
                            return self.navigator(str(q1), stype, None, downloadPath, overwrite, home, nlist)
                                
                        if q2 and isinstance(q2, list):
                            for s in q2:
                                if str(s).isdigit():
                                    name = data_result.get(int(s)).get(
                                        'longname')
                                    debug(process = "q2 and isinstance(q2, list)", name = name, download_link = data_result.get(int(s)).get(
                                        'download_link'))
                                    self.download(data_result.get(int(s)).get(
                                        'download_link'), downloadPath, overwrite, name=name)
                            #return self.navigator(query, stype, url_query, downloadPath, overwrite, proxy, home, nlist, verify)
                            return self.navigator(query, stype, url_query, downloadPath, overwrite, home, nlist, proxy=proxy)
                        else:
                            if q2 and str(q2).isdigit():
                                self.download(self.URL + data_result.get(
                                    int(q2) - 1).get('download_link'), downloadPath, overwrite, name=data_result.get(
                                    int(q2) - 1).get('longname'))
                                #return self.navigator(query, stype, url_query, downloadPath, overwrite, proxy, home, nlist, verify)
                                return self.navigator(query, stype, url_query, downloadPath, overwrite, home, nlist, proxy=proxy)
                            elif q2 and str(q2).lower() == 'all' or str(q2).lower() == 'a':
                                print "download all"
                                for i in data_result:
                                    sys.stdout.write(str(i) + ". ")
                                    # print "i        =",i
                                    # print "url      =",url
                                    self.download(self.URL + data_result.get(
                                        i).get('download_link'), downloadPath, name=data_result.get(
                                        i).get('longname'))
                                #return self.navigator(query, stype, url_query, downloadPath, overwrite, proxy, home, nlist, verify)
                                return self.navigator(query, stype, url_query, downloadPath, overwrite, home, nlist, proxy=proxy)
                            else:
                                return self.navigator(str(q1), stype, url_query, downloadPath, overwrite, False, nlist, proxy=proxy)
                else:
                    #debug(self_page2 = self.page)
                    if q1 == '':
                        return self.navigator(None, stype, url_query, downloadPath, overwrite, home, nlist, self.page, proxy)                   
                    debug(url_query = url_query)
                    return self.navigator(q1, stype, url_query, downloadPath, overwrite, home, nlist, self.page, proxy)                   
            else:
                if page:
                    page_list0 = []
                    page_list = ""
                    for i in page:
                        page_list0.append(i)
                    page_list0 = sorted(page_list0)
                    for i in page_list0:
                        if str(i).isdigit():
                            page_list += str(i) + " | "
                    page_list = "  p[revious] " + page_list + " n[ext]"
                    print page_list
                    q1 = raw_input('[%s:%s] Select Number to Download [p = previous, n = next, x = goto page, c = change category, s = search, h = home, q = quit]: ' % (PID, MEM))
                elif page_return:
                    page = page_return
                    page_list0 = []
                    page_list = ""
                    for i in page:
                        page_list0.append(i)
                    page_list0 = sorted(page_list0)
                    for i in page_list0:
                        if str(i).isdigit():
                            page_list += str(i) + " | "
                    page_list = "  p[revious] " + page_list + " n[ext]"
                    print page_list
                    q1 = raw_input('[%s:%s] Select Number to Download [p = previous, n = next, x = goto page, c = change category, s = search, h = home, q = quit]: ' % (PID, MEM))
                else:
                    q1 = raw_input('[%s:%s] Select Number to Download [c = goto category of page, s = search, h = home, N[number of list] = number each of list, q = quit]: ' % (PID, MEM))
    
    def usage(self):
        import argparse
        parser = argparse.ArgumentParser(formatter_class= argparse.RawTextHelpFormatter)
        parser.add_argument('-s', '--search', help = 'Search query', action = 'store', default = '')
        parser.add_argument('-H', '--home', help = 'Show Home Page', action = 'store_true')
        parser.add_argument('-d', '--download-path', help = 'Directory of download of torrent files', action = 'store', default = r'f:\TORRENT_FILES')
        parser.add_argument('-c', '--category', help = 'Set category before', action = 'store')
        parser.add_argument('-n', '--nlist', help = 'Numbers of Row of list', action = 'store', default = 3, type = int)
        parser.add_argument('-X', '--proxy', help = 'Use proxy for connection, example: https://127.0.0.1:80 http://10.30.4.5:3128 or just type "auto" for auto proxy', action = 'store', nargs = '*')
        parser.add_argument('-x', '--use-proxy', help = 'Use Auto proxy for connection or your can given with -X --proxy options ', action = 'store_true')
        parser.add_argument('-Y', '--no-verify', help='ByPass Veriy SSL for https only', action='store_false')
        parser.add_argument('-v', '--debug', help='Debug Process (Development)',action='store_true')
        parser.add_argument('-o', '--overwrite', help = 'Overwrite download file exists', action = 'store_true')
        if len(sys.argv) == 1:
            parser.print_help()
            args = parser.parse_args()
            if args.debug:
                self.debug = args.debug
            #if args.no_verify:
                #self.VERIFY = args.no_verify
            if args.proxy:
                if args.proxy[0] == 'auto':
                    proxy = 'auto'
                else:
                    proxy = self.setProxy(args.proxy)
            else:
                proxy = None
            if not args.no_verify:
                self.navigator(args.search, args.category, None, args.download_path, args.overwrite, True, args.nlist, verify=args.no_verify, proxies=proxy)
            else:
                self.navigator(args.search, args.category, None, args.download_path, args.overwrite, True, args.nlist, proxy=proxy)
        else:
            args = parser.parse_args()

            # if args.search:
            #     self.search(args.search)

            if args.use_proxy:
                proxy = self.setProxy()
                debug(proxy=proxy)

            if args.debug:
                self.debug = args.debug
            if args.no_verify:
                self.VERIFY = args.no_verify
            if args.proxy:
                if args.proxy[0] == 'auto':
                    proxy = 'auto'
                else:
                    proxy = self.setProxy(args.proxy)
            else:
                proxy = {}
            debug(proxy=proxy)
            if not args.no_verify:
                self.navigator(args.search, args.category, None, args.download_path, args.overwrite, args.home, args.nlist, verify=args.no_verify, proxy=proxy)
            else:
                self.navigator(args.search, args.category, None, args.download_path, args.overwrite, True, args.nlist, proxy=proxy)
        
if __name__ == '__main__':
    c = limetorrents()
    c.usage()
    # c.getDetails('https://www.limetorrents.info/The-Nutcracker-And-The-Four-Realms-(2018)-[BluRay]-[720p]-[YTS-AM]-torrent-12109026.html')
    #c.home(stype= 'movies')
    #if len(sys.argv) == 1:
        #c.navigator('', None, home= True, downloadPath= r'f:\TORRENT_FILES')
    #else:
        #c.navigator(sys.argv[1], None, home= True, downloadPath= r'f:\TORRENT_FILES')
